
package uma.easy.rules.showcase;

import com.github.selwynshen.nics.rules.api.Facts;
import com.github.selwynshen.nics.rules.api.Rules;
import com.github.selwynshen.nics.rules.api.RulesEngine;
import com.github.selwynshen.nics.rules.core.DefaultRulesEngine;

import org.junit.Before;
import org.junit.Test;

import org.springframework.util.ResourceUtils;


import java.io.*;

import com.github.selwynshen.nics.rules.mvel.MVELRuleFactory;



/**
 * @author Uma
 * @version $Id: PriorityTest.java, v 0.1 11/24/2018 4:29 PM Uma Exp $
 */

public class RulesTest {

	//private MVELRuleFactory ruleFactory;
	  private RulesEngine rulesEngine;

    @Before
    public void init()
    {

    	System.out.println("yes");
    	 this.rulesEngine = new DefaultRulesEngine();
    }

    
    @Test
    public void testYamlRead() throws Exception
    {

        Rules ruls;
        File file = ResourceUtils.getFile("classpath:rules123.yml");
        ruls= MVELRuleFactory.createRulesFrom(new FileReader(file));
        System.out.println("rules done");
        System.out.println(ruls.toString());

        
        Facts fct = new Facts();
        fct.put("rain", "true");
        fct.put("age", "19");
       
        
        this.rulesEngine.fire(ruls, fct);
        
        System.out.println("done firing done");
    }
}
