package uma.easy.rules.showcase.data;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

/**
 * @author Uma
 * @version $Id: Invoice.java, v 0.1 3/29/2019 7:20 PM Uma Exp $
 */
@Data
public class FindRulesEngine {
    
    private String number;
    
    private String pickedRule;
}
