package uma.easy.rules.showcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PecosEasyRulesShowcaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(PecosEasyRulesShowcaseApplication.class, args);
	}

}
