package uma.easy.rules.showcase.manager;

import uma.easy.rules.showcase.data.FindRulesEngine;
import uma.easy.rules.showcase.data.VerificationCheckList;
import uma.easy.rules.showcase.service.VerificationCheckListService;
import nics.easy.rules.spring.facts.AdvFacts;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import com.github.selwynshen.nics.rules.api.Facts;
import com.github.selwynshen.nics.rules.api.Rules;
import com.github.selwynshen.nics.rules.api.RulesEngine;
import com.github.selwynshen.nics.rules.core.DefaultRulesEngine;
import com.github.selwynshen.nics.rules.mvel.MVELRuleFactory;

import java.io.File;
import java.io.FileReader;

import javax.annotation.Resource;

/**
 * @author Uma
 * @version $Id: verificationManager.java, v 0.1 3/29/2019 7:39 PM Uma Exp $
 */
@Service
public class VerificationManager {

    @Resource
    private VerificationCheckListService verificationCheckListService;
    
    private RulesEngine rulesEngine;
    private Rules ruls;
    //private AdvFacts facts;
    private Facts fact,facts;
    private String fileNames;
    private FindRulesEngine fre;
    private VerificationManager() throws Exception {
    	yamlRead("classpath:rulesmaster.yml");
    }
    public void checkVerification(VerificationCheckList verificationList) 
    {
        this.facts=this.verificationCheckListService.verificationCheckList(verificationList);
        fre = new FindRulesEngine();
        if(verificationList.getWhichRulePick()!=null) {
        fre.setNumber(verificationList.getWhichRulePick());
        
        String fileNameRules=prepareInitialFacts();
        //this.fileNames = fre.getPickedRule();
        if (fileNameRules!=null) {
        	 try {
     			yamlRead(fileNameRules);
     		} catch (Exception e) {
     			System.out.println("Rules file not found");
     		}
             this.rulesEngine.fire(ruls, facts);

        }
    }
    }
    
    private void initializeRules() {
    	this.rulesEngine = new DefaultRulesEngine();
    }

    private void yamlRead(String fileName) throws Exception
    {
    	initializeRules();
    	File file = ResourceUtils.getFile(fileName);
        ruls= MVELRuleFactory.createRulesFrom(new FileReader(file));

    }

    
  
    private String  prepareInitialFacts() {
    	fact = new Facts();
    	fact.put("yearfile",this.fre.getNumber());
   	this.rulesEngine.fire(ruls, this.fact);
   	System.out.println(" rulesfile picked is "+fact.get("yearfile"));
   	return fact.get("yearfile");
    }

}
