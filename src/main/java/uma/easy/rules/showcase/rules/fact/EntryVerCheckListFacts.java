package uma.easy.rules.showcase.rules.fact;


import uma.easy.rules.showcase.data.VerificationCheckList;
import nics.easy.rules.spring.facts.AdvFacts;

/**
 * @author Uma
 * @version $Id: EntryCheckFacts.java, v 0.1 3/29/2019 7:16 PM Uma Exp $
 */
public class EntryVerCheckListFacts extends AdvFacts{

    private static final String KEY_CHECKLIST = "verificationCheckList";

    public void putVerification(VerificationCheckList verificationList)
    {
        this.put(KEY_CHECKLIST, verificationList);
    }

    public VerificationCheckList getVerification()
    {
        return this.get(KEY_CHECKLIST);
    }
}
