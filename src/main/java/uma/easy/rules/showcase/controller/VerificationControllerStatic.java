package uma.easy.rules.showcase.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import uma.easy.rules.showcase.data.VerificationCheckList;
import uma.easy.rules.showcase.manager.VerificationManager;
import uma.easy.rules.showcase.manager.VerificationManagerStatic;
import uma.easy.rules.showcase.service.VerificationCheckListService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Uma
 * @version $Id: VerificationController.java, v 0.1 3/29/2019 7:39 PM Uma Exp $
 */
@RestController
@RequestMapping("/verification/checkstatic")
@Api(value = "Provides verification", consumes = "application/json,application/xml")
public class VerificationControllerStatic {
	@Resource
	private VerificationManagerStatic verificationMgr;
	
    @Resource
    private VerificationCheckListService verificationCheckListService;

    
    @PostMapping
    @ApiOperation(value = "verification check", notes = "Accepted Values::submissionReason=1 or 2//formType=855 or 856")
    public void checkVerification(@ApiParam(value="Verification Information") @RequestBody VerificationCheckList verificationList)
    {
        this.verificationMgr.checkVerification(verificationList);
    }

}
