package uma.easy.rules.showcase.service;

import uma.easy.rules.showcase.data.VerificationCheckList;

import com.github.selwynshen.nics.rules.api.Facts;

import nics.easy.rules.spring.facts.AdvFacts;;

/**
 * @author Uma
 * @version $Id: InvoiceCheckService.java, v 0.1 3/29/2019 7:38 PM Uma Exp $
 */
public interface VerificationCheckListService {

	Facts verificationCheckList(VerificationCheckList verificationList);
}
