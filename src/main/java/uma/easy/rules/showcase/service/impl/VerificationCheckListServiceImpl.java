package uma.easy.rules.showcase.service.impl;

import org.springframework.stereotype.Service;

import com.github.selwynshen.nics.rules.api.Facts;

import lombok.extern.slf4j.Slf4j;
import uma.easy.rules.showcase.data.VerificationCheckList;
import uma.easy.rules.showcase.rules.fact.EntryVerCheckListFacts;
import uma.easy.rules.showcase.service.VerificationCheckListService;
import nics.easy.rules.spring.facts.AdvFacts;

/**
 * @author Uma
 * @version $Id: InvoiceCheckServiceImpl.java, v 0.1 3/29/2019 7:44 PM Uma Exp $
 */
@Service
@Slf4j
public class VerificationCheckListServiceImpl implements VerificationCheckListService {

    private Facts fact;


	@Override
	public Facts verificationCheckList(VerificationCheckList verificationList) {
		// TODO Auto-generated method stub
      //  EntryVerCheckListFacts facts = new EntryVerCheckListFacts();
      //  facts.putVerification(verificationList);
		fact = new Facts();
		fact.put("formtype", verificationList.getFormType());
		fact.put("submitrsn", verificationList.getSubmissionReason());
        return  fact;
	}
}
