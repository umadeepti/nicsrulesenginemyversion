package uma.easy.rules.showcase.configurer;

import nics.easy.rules.spring.support.ProtoRulesEngineFactoryBean;
import nics.easy.rules.spring.support.RulesDefinitionFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author uma
 * @version $Id: EasyRulesConfigurer.java, v 0.1 1/3/2019 3:19 PM uma Exp $
 */
@Configuration
public class EasyRulesConfigurer {

    @Bean
    ProtoRulesEngineFactoryBean rulesEngineFactoryBean()
    {
        return new ProtoRulesEngineFactoryBean();
    }

    @Bean
    @ConfigurationProperties(prefix = "uma.easyrules")
    RulesDefinitionFactory rulesDefinitionFactory()
    {
        return new RulesDefinitionFactory();
    }

}
